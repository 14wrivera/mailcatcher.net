﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MailCatcher.Net.Web.Enumerations
{
    public enum AlertTypes
    {
        Success,
        Warning,
        Info,
        Error
    }
}