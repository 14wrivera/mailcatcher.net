﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using MailCatcher.Net.MailMessage.MimeTypes;

namespace MailCatcher.Net.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Download content
        /// </summary>
        /// <param name="message">the message</param>
        /// <returns></returns>
        public ActionResult Download(MessagePart message)
        {
            message.Contents= new StringBuilder(this.Request.Form["ContentsData"] as String);
            string fileName = "unknown";
            if (message.ContentDisposition != null && message.ContentDisposition.ToLower().Contains("filename="))
            {
                Match match = Regex.Match(message.ContentDisposition.ToLower(),
                    "filename\\=\"{0,1}(.*)(?:[\"]{0,1})");
                fileName = match.Groups[1].Value.Replace("\"", String.Empty); ;
            }
            else if (message.ContentType.ToLower().Contains("name="))
            {
                Match match = Regex.Match(message.ContentType.ToLower(),
                    "name\\=\"{0,1}(.*)(?:[\"]{0,1})");
                fileName = match.Groups[1].Value.Replace("\"", String.Empty); ;
            }

            MemoryStream ms = RetrieveFileBytes(message);
            return File(ms, message.ContentType, fileName);
        }
        /// <summary>
        /// attempt to get the bytes
        /// </summary>
        /// <param name="message">the message</param>
        /// <returns></returns>
        private MemoryStream RetrieveFileBytes(MessagePart message)
        {
            byte[] bytes;
            try
            {
                bytes = Convert.FromBase64String(message.RetrieveContents());
            }
            catch
            {
                var str = message.RetrieveContents();
                bytes = new byte[str.Length * sizeof(char)];
                System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            }
            MemoryStream ms = new MemoryStream(bytes);
            return ms;
        }
    }
}