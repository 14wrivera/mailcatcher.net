﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using MailCatcher.Net.MailMessage;
using MailCatcher.Net.MailMessage.Interfaces;
using MailCatcher.Net.Web.App_Start;

namespace MailCatcher.Net.Web.Models
{
    public class SmtpMessage
    {
        #region Properties
        public string Message { get; set; }
        public string To { get; set; }
        public string From { get; set; }
        public string Subject { get; set; }
        public string Date { get; set; }

        public string HTMLMessage { get; set; }
        public string PlainMessage { get; set; }
        public List<IMimeParts> Attachments { get; set; }
        #endregion

        /// <summary>
        /// Base Constructor
        /// </summary>
        /// <param name="message">The Overall SmtpMessage</param>
        public SmtpMessage(List<string> message): base()
        {
            this.Message = string.Join( "\n", message);
            int len = 50;
            if (message.Count < 50)
            {
                len=message.Count -1;
            }
            for (int i = 0; i < len; i++)
            {
                string msg = message[i];
                if (this.Subject == null && msg.StartsWith("Subject:"))
                    this.Subject = msg.Substring(8);
                if (this.Date == null && msg.StartsWith("Date:"))
                    this.Date = msg.Substring(5);
            }
        }

        /// <summary>
        /// update the properties of the message
        /// </summary>
        public void UpdateParts()
        {
            List<IMimeParts> attachments = new List<IMimeParts>();
            MessageParts parts = new MessageParts(this.Message);
            attachments = parts.RetrieveAttachments().ToList();


            IMimeParts res = parts.FindByContentType("text/plain");
            string text = "No Plain Content";
            if (res != null && res.Contents != null)
            {
                text = res.RetrieveContents();
            }

            this.PlainMessage = text;

            res = parts.FindByContentType("text/html");
            text = "<CENTER>No HTML Content</CENTER>";
            if (res != null && res.Contents != null)
            {
                text = res.RetrieveContents();
                List<IMimeParts> removable = new List<IMimeParts>();
                foreach (string t in text.Split(new string[1] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries))
                {
                    Match match = Regex.Match(t, "src=[\"|\']{1}(.+)[\"|\']{1}", RegexOptions.IgnoreCase);
                    if (match.Success)
                    {
                        string id = match.Groups[1].Value;
                        text = attachments.FindAndUpdate(id, text, removable);
                    }
                }
                attachments = attachments.Except(removable).ToList();
                this.HTMLMessage = text;
            }

            this.Attachments = attachments;
        }
    }
}