﻿using MailCatcher.Net.Web.Enumerations;
using MailCatcher.Net.Web.Models;
using Microsoft.AspNet.SignalR;

namespace MailCatcher.Net.Web.App_Start
{
    public class ApplicationHub: Hub
    {
        public void Send(AlertTypes type, string message)
        {
            Clients.All.Notification(type, message);
        }

        public void MailMessage(SmtpMessage msg)
        {
            Clients.All.IncomingMail(msg);
        }
    }
}