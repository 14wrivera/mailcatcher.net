﻿using Microsoft.Owin;
using Owin;

namespace MailCatcher.Net.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}
