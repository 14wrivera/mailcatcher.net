﻿using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using MailCatcher.Net.Server;
using MailCatcher.Net.Server.EventArgs;
using MailCatcher.Net.Web.App_Start;
using MailCatcher.Net.Web.Enumerations;
using MailCatcher.Net.Web.Models;
using Microsoft.AspNet.SignalR;

namespace MailCatcher.Net.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private static TaskScheduler _appContext;
        private static MailCatcherServer _server;

        static MvcApplication()
        {
            _server = new MailCatcherServer();
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            this.StartSMTPServer();
        }

        #region Private Methods
        private void StartSMTPServer()
        {
            try{
                var ipAddress = ConfigurationManager.AppSettings["IP"].ToString();
                var portconfig =ConfigurationManager.AppSettings["Port"].ToString();

                var ip = IPAddress.Parse(ipAddress );
                var port = Convert.ToInt32(portconfig);

                _server.IpAddress = ip;
                _server.Port = port;
                _server.OnMessageReceived += this.serverOnComplete;

                GlobalHost.ConnectionManager.GetHubContext<ApplicationHub>().Clients.All.Notification(AlertTypes.Success, String.Format("Server is running on {0}:{1}", ip.ToString(), port.ToString()));
                this.StartServer();
            }catch(Exception e ){
                string message= String.Format("There was a problem starting the _server: {0}",e.Message);
                GlobalHost.ConnectionManager.GetHubContext<ApplicationHub>().Clients.All.Notification(AlertTypes.Error, message);
                this.StopServer();
            }
        }
        /// <summary>
        /// Server on stop read Method, Delegate declared in SmtpServer
        /// </summary>
        /// <param name="inputText">The actual text</param>
        /// <param name="state">The Server State</param>
        /// <param name="msgState">The message state</param>
        /// <remarks>This method automatically runs on a
        /// different thread</remarks>
        private void serverOnComplete(Object sender, MessageEventArgs arg)
        {
            //update the UI Thread
            new Task(() =>
            {
                GlobalHost.ConnectionManager.GetHubContext<ApplicationHub>().Clients.All.Notification(AlertTypes.Success, "Reading Incoming SmtpMessage...");
                if (arg.Message.Count > 0)
                {
                    SmtpMessage item = new SmtpMessage(arg.Message.ToList());

                    arg.Headers.ToList().ForEach(itm =>
                    {
                        if (itm.StartsWith("MAIL FROM"))
                            item.From = itm.Substring(10).Trim();
                        if (itm.StartsWith("RCPT TO"))
                            item.To += itm.Substring(8).Trim() + "; ";
                    });
                    item.To = item.To.Remove(item.To.Length - 2);
                    item.UpdateParts();
                    
                    GlobalHost.ConnectionManager.GetHubContext<ApplicationHub>().Clients.All.IncomingMail(item);
                }
            }).Start(_appContext);
        }
        #endregion
        #region Public Methods
        /// <summary>
        /// Start the Server and
        /// set up the events
        /// </summary>
        public void StartServer()
        {
            _appContext = TaskScheduler.Default;
            try
            {
                _server.Start();
            }
            catch (SocketException e)
            {
                GlobalHost.ConnectionManager.GetHubContext<ApplicationHub>().Clients.All.Notification(AlertTypes.Error, String.Format("SocketException: {0}", e.SocketErrorCode));
                this.StopServer();
            }
        }

        /// <summary>
        /// Stop the _server
        /// </summary>
        public void StopServer()
        {
            _server.Stop();
            GlobalHost.ConnectionManager.GetHubContext<ApplicationHub>().Clients.All.Notification(AlertTypes.Success, "Server Stopped");
        }
        #endregion
    }
}
