﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using MailCatcher.Net.MailMessage;
using MailCatcher.Net.MailMessage.Interfaces;
using MailCatcher.Net.MailMessage.MimeTypes;
using MailCatcher.Net.Model;
using MailCatcher.Net.Server;
using MailCatcher.Net.Server.EventArgs;
using MailCatcher.Net.wpf.Extensions;
using MailCatcher.Net.wpf.Interfaces;
using MailCatcher.Net.wpf.Model;
using Microsoft.Win32;

namespace MailCatcher.Net.wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IApplicationWindow
    {
        #region Members
        private TaskScheduler context;
        private MailCatcherServer server;
        private IPAddress ip;
        private int port;
        #endregion

        #region Constructor
        /// <summary>
        /// Start the application
        /// </summary>
        public MainWindow()
        {
            InitializeComponent(); 
            this.ipMenu.AppWindow = this; 
            try{
                var ipAddress = ConfigurationManager.AppSettings["IP"].ToString();
                var port =ConfigurationManager.AppSettings["Port"].ToString();

                this.ip = IPAddress.Parse(ipAddress );
                this.port = Convert.ToInt32(port);
                this.server = new MailCatcherServer(this.ip, this.port);

                ((INotifyCollectionChanged)this.lst.Items).CollectionChanged += this.Added;
                this.lst.SelectionChanged += this.SelectionChanged;
                this.server.OnMessageReceived += this.serverOnComplete;
                this.server.OnMessageStart += this.OnMessageReading;
                this.StartServer();
            }catch{
                string message= "There was a problem starting the server";
                MessageBox.Show(message);
                this.ErrorNotification(message);
                this.StopServer();
            }

            
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Start the Server and
        /// set up the events
        /// </summary>
        public void StartServer()
        {
            this.RunningNotification.Text =
                String.Format("Server is running on {0}:{1}",this.ip.ToString(),this.port.ToString());
            this.IssueNotification.Text = "No Issues";
            this.context = TaskScheduler.FromCurrentSynchronizationContext();
            try
            {
                this.server.Start();
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
                this.IssueNotification.Text = String.Format("SocketException: {0}", e.SocketErrorCode);
                this.IssueNotification.ToolTip = e.Message;
                this.StopServer();
                MessageBox.Show(e.Message, "Network Error");
            }
        }

        /// <summary>
        /// Start the server on a specified port and ip address
        /// </summary>
        /// <param name="address">the ip address to use</param>
        /// <param name="port">the port to use</param>
        public void StartServer(IPAddress address, int port)
        {
            this.ip = address;
            this.port = port;
            this.server.IpAddress = this.ip;
            this.server.Port = this.port;
            this.StartServer();
        }

        /// <summary>
        /// Stop the server
        /// </summary>
        public void StopServer()
        {
            this.server.Stop();
            this.RunningNotification.Text = "Server Stopped";
        }

        /// <summary>
        /// update the application notification with any errors
        /// </summary>
        /// <param name="message">the message</param>
        public void ErrorNotification(string message)
        {
            this.IssueNotification.Text = message;
            this.IssueNotification.ToolTip = this.IssueNotification.Text;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// On message start event
        /// </summary>
        /// <param name="sender">the MailCatcher Server</param>
        /// <param name="args">system event args</param>
        private void OnMessageReading(object sender, System.EventArgs args)
        {
            //update the UI Thread
            new Task(() =>
            {
                this.IssueNotification.Text = "Reading Incoming Message...";
            }).Start(this.context);
        }
        /// <summary>
        /// Server on stop read Method, Delegate declared in SmtpServer
        /// </summary>
        /// <param name="inputText">The actual text</param>
        /// <param name="state">The Server State</param>
        /// <param name="msgState">The message state</param>
        /// <remarks>This method automatically runs on a
        /// different thread</remarks>
        private void serverOnComplete(Object sender, MessageEventArgs arg)
        {
            //update the UI Thread
            new Task(() =>
            {
                if (arg.Message.Count > 0)
                {
                    ListViewMessage item = new ListViewMessage(arg.Message.ToList());

                    arg.Headers.ToList().ForEach(itm =>
                    {
                        if (itm.StartsWith("MAIL FROM"))
                            item.From = itm.Substring(10).Trim();
                        if (itm.StartsWith("RCPT TO"))
                            item.To += itm.Substring(8).Trim() + "; ";
                    });
                    item.To = item.To.Remove(item.To.Length - 2);
                    this.lst.Items.Add(item);
                    this.lst.SelectedItem = item;
                    this.IssueNotification.Text = "No Issues";
                }
            }).Start(this.context);
        }

        /// <summary>
        /// List items update event
        /// </summary>
        /// <param name="message">The Event Object</param>
        /// <param name="args">Event Args</param>
        private void SelectionChanged(Object message, SelectionChangedEventArgs args)
        {
            if (this.lst.SelectedItem != null)
            {
                ListViewMessage selected = (ListViewMessage)this.lst.SelectedItem;
                this.updateSelection(selected);
            }
        }

        private void SortList(Object message, RoutedEventArgs args)
        {
            string columnClicked = ((GridViewColumnHeader)args.OriginalSource).Column.Header.ToString();
            var direction = (this.lst.Items.SortDescriptions.SingleOrDefault().Direction == ListSortDirection.Ascending) ?
                        ListSortDirection.Descending : ListSortDirection.Ascending;

            this.lst.Items.SortDescriptions.Clear();
            SortDescription sd = new SortDescription(columnClicked, direction);
            this.lst.Items.SortDescriptions.Add(sd);
            this.lst.Items.Refresh();
        }

        /// <summary>
        /// Updates the raw input and the headers section
        /// </summary>
        /// <param name="selected">is a <typeparamref name="ListViewMessage"/>.</param>
        private void updateSelection(ListViewMessage selected)
        {
            this.fromBox.Text = "From: " + selected.From;
            this.toBox.Text = "To: " + selected.To;
            this.subjectBox.Text = "Subject: " + selected.Subject;
            this.dateBox.Text = "Date: " + selected.Date;
            this.updateParts(selected);
        }

        /// <summary>
        /// This method updates the tabs
        /// </summary>
        /// <param name="selected">is a <typeparamref name="ListViewMessage"/>.</param>
        private void updateParts(ListViewMessage selected)
        {
            List<IMimeParts> attachments = new List<IMimeParts>();
            if (selected.PlainMessage == null)
            {
                MessageParts parts = new MessageParts(selected.Message);
                attachments = parts.RetrieveAttachments().ToList();
                selected.PlainMessage = this.UpdatePlainTab(parts);
                selected.HTMLMessage = this.UpdateHTMLTab(parts, ref attachments);
            }

            this.rawInput.Text = selected.Message;
            this.plainInput.Text = selected.PlainMessage;
            this.htmlInput.NavigateToString(selected.HTMLMessage); 
            this.RetrieveAttachments(attachments);
        }

        /// <summary>
        /// Updates the plain in the html tab
        /// </summary>
        /// <param name="parts">is a<typeofparam name="parts">MessageParts</typeofparam>. the message parts</param>
        private string UpdatePlainTab(MessageParts parts)
        {
            MessagePart res = parts.FindByContentType("text/plain") as MessagePart;
            string text = "No Plain Content";
            if (res != null && res.Contents != null)
            {
                text = res.RetrieveContents();
            }
            return text;
        }

        /// <summary>
        /// Updates the html in the html tab
        /// </summary>
        /// <param name="parts">is a<typeofparam name="parts">MessageParts</typeofparam>. the message parts</param>
        private string UpdateHTMLTab(MessageParts parts, ref List<IMimeParts> attachements)
        {
            MessagePart res = parts.FindByContentType("text/html") as MessagePart;
            string text = "<CENTER>No HTML Content</CENTER>";
            if (res !=null && res.Contents != null)
            {
                text = res.RetrieveContents();
                text = UpdateImage(ref attachements, text);
            }
            return text;
        }

        /// <summary>
        /// Updates images in html text
        /// </summary>
        /// <param name="attachements">the attachments to search through</param>
        /// <param name="text">the text to search</param>
        /// <returns>the updated text</returns>
        private static string UpdateImage(ref List<IMimeParts> attachements, string text)
        {
            List<IMimeParts> removable = new List<IMimeParts>();
            foreach (string t in text.Split(new string[1] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries))
            {
                Match match = Regex.Match(t, "src=[\"|\']{1}(.+)[\"|\']{1}", RegexOptions.IgnoreCase);
                if (match.Success)
                {
                    string id = match.Groups[1].Value;
                    text = attachements.FindAndUpdate(id,text,removable);
                }
            }
            attachements = attachements.Except(removable).ToList();
            return text;
        }

        /// <summary>
        /// Updates the attachments 
        /// </summary>
        /// <param name="parts">is a<typeofparam name="MessageParts"> parts 
        /// from the message</typeofparam>. 
        /// the message parts</param>
        private void RetrieveAttachments( List<IMimeParts> attachments )
        {
            this.AttachmentsBox.Inlines.Clear();
            if (attachments.Count>0 && attachments[0].ContentType != null)
            {
                this.AttachmentsBox.Inlines.Add(new Run("Attachments: "));
                int i = 0;
                foreach (IMimeParts part in attachments)
                {
                    this.AddAttachment(part,i);
                    i++;
                }
            }
        }

        /// <summary>
        /// adds a click event to an attachment
        /// </summary>
        /// <param name="parts">is a<typeofparam name="IMimePart">.</typeofparam>. 
        ///  this is 
        /// the attachment</param>
        /// <param name="i">The index</param>
        private void AddAttachment(IMimeParts part, int i)
        {
            string fileName = "unknown";
            if (part.ContentDisposition!=null && part.ContentDisposition.ToLower().Contains("filename="))
            {
                Match match = Regex.Match(part.ContentDisposition.ToLower(),
                   "filename\\=\"{0,1}(.*)(?:[\"]{0,1})"); 
                fileName = match.Groups[1].Value.Replace("\"", String.Empty); ;
            }
            else if (part.ContentType.ToLower().Contains("name="))
            {
                Match match = Regex.Match(part.ContentType.ToLower(),
                    "name\\=\"{0,1}(.*)(?:[\"]{0,1})");
                fileName = match.Groups[1].Value.Replace("\"", String.Empty); ;
            }

            string runName = fileName;
            if (i > 0)
                runName = ", " + runName;

            TextBlock content = new TextBlock(new Run(runName));
            content.Cursor = Cursors.Hand;
            content.Foreground = Brushes.Blue;
            content.TextDecorations = TextDecorations.Underline;
            this.AssigneClickEvent(part, fileName, content);
            this.AttachmentsBox.Inlines.Add(content);
        }
        /// <summary>
        /// Present save dialog
        /// </summary>
        /// <param name="part">is a<typeofparam name="IMimePart"></param>
        /// <param name="name">The file name</param>
        /// <param name="content">the TextBlock that we are assigning the
        /// click even to</param>
        private void AssigneClickEvent(IMimeParts part, string name, TextBlock content)
        {
            content.MouseDown += (obj, args) =>
            {
                SaveFileDialog dlg = new SaveFileDialog();
                dlg.FileName = name; // Default file name

                // Show save file dialog box
                Nullable<bool> result = dlg.ShowDialog();
                
                string filename = dlg.FileName;
                // Process save file dialog box results
                if (result == true)
                {
                    byte[] b;
                    b = Convert.FromBase64String(part.RetrieveContents());
                    MemoryStream ms = new MemoryStream(b);
                    using(FileStream fs = new FileStream(filename,FileMode.Create))
                    {
                        ms.WriteTo(fs);
                    }
                }
            };
        }

        /// <summary>
        /// List items update event
        /// </summary>
        /// <param name="message">The Event Object</param>
        /// <param name="args">Event Args</param>
        private void Added(Object message, NotifyCollectionChangedEventArgs args)
        {
            if (args.Action == NotifyCollectionChangedAction.Add)
            {
                ListViewMessage selected = (ListViewMessage)args.NewItems[0];
                this.updateSelection(selected);
            }
        }

        /// <summary>
        /// Stop the Server
        /// </summary>
        /// <param name="sender">Sending Object</param>
        /// <param name="e">Event Arguments</param>
        private void SettingsMenuRestartServerClick(object sender, EventArgs e)
        {
            this.StopServer();
            this.StartServer();
        }

        /// <summary>
        /// Stop the Server
        /// </summary>
        /// <param name="sender">Sending Object</param>
        /// <param name="e">Event Arguments</param>
        private void SettingsMenuStopServerClick(object sender, EventArgs e)
        {
            this.StopServer();
        }

        /// <summary>
        /// Clears all messages
        /// </summary>
        /// <param name="sender">Sending Object</param>
        /// <param name="e">Event Arguments</param>
        private void ClearMessageEvent(object sender, EventArgs e)
        {
            this.lst.Items.Clear();
            this.rawInput.Text = "";
            this.plainInput.Text = "";
            this.htmlInput.Source = null;
            this.ClearHeaderDisplay();
        }

        /// <summary>
        /// Validates that the clear message option should show
        /// </summary>
        /// <param name="sender">object as context menu</param>
        /// <param name="e">the on open event</param>
        private void ValidateOption(object sender, EventArgs e)
        {
            this.lst.ContextMenu.Visibility= Visibility.Visible;
            if(lst.Items.Count==0)
                this.lst.ContextMenu.Visibility = Visibility.Hidden;
            

            if (this.lst.SelectedItem != null)
            {
                if (this.lst.ContextMenu.Items.Count == 1)
                {
                    TextBlock content = new TextBlock(new Run("Clear Message"));
                    content.Name = "DelMesg";
                    content.MouseDown += DeleteMessage;
                    this.lst.ContextMenu.Items.Add(content);

                    content = new TextBlock(new Run("Download Message"));
                    content.Name = "DownloadMessage";
                    content.MouseDown += DwnldMessageEvent;
                    this.lst.ContextMenu.Items.Add(content);

                }
            }
            else
            {
                if (this.lst.ContextMenu.Items.Count > 1)
                {
                    this.lst.ContextMenu.Items.RemoveAt(2);
                    this.lst.ContextMenu.Items.RemoveAt(1);
                }
            }
        }

        /// <summary>
        /// clears out the message header information on the display
        /// </summary>
        private void ClearHeaderDisplay()
        {
            this.fromBox.Text = "From: " ;
            this.toBox.Text = "To: ";
            this.subjectBox.Text = "Subject: ";
            this.dateBox.Text = "Date: ";
            this.AttachmentsBox.Text = "";
        }

        /// <summary>
        /// Downloads the selected message
        /// </summary>
        /// <param name="sender">Sending Object</param>
        /// <param name="e">Event Arguments</param>
        private void DwnldMessageEvent(object sender, EventArgs e)
        {
            ListViewMessage selected = (ListViewMessage)this.lst.SelectedItem;
            var dialog = new SaveFileDialog();
            dialog.Filter="Email Files |*.eml";
            if (dialog.ShowDialog() == true)
            {
                File.WriteAllText(dialog.FileName, selected.Message);
            }

        }

        /// <summary>
        /// Clears all messages
        /// </summary>
        /// <param name="sender">Sending Object</param>
        /// <param name="e">Event Arguments</param>
        private void DeleteMessage(object sender, EventArgs e)
        {
            this.lst.Items.Remove(this.lst.SelectedItem);
            this.rawInput.Text = "";
            this.plainInput.Text = "";
            this.htmlInput.Source = null;
            if (lst.Items.Count == 0)
            {
                this.ClearMessageEvent(sender, e);
            }
            else
            {
                this.lst.Items.Refresh();
                this.ClearHeaderDisplay();
            }
        }
        

        /// <summary>
        /// Show the Console
        /// </summary>
        /// <param name="sender">Sending Object</param>
        /// <param name="e">Event Arguments</param>
        private void ShowConsoleClick(object sender, RoutedEventArgs e)
        {
            //this.NotificationWindow.IsOpen = true;
            ConsoleManager.Show();
        }

        /// <summary>
        /// hides the console
        /// </summary>
        /// <param name="sender">Sending Object</param>
        /// <param name="e">Event Arguments</param>
        private void HideConsoleClick(object sender, RoutedEventArgs e)
        {
            ConsoleManager.Hide();
        }
        #endregion
    }
}