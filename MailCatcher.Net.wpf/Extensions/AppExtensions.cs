﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using MailCatcher.Net.MailMessage.Interfaces;

namespace MailCatcher.Net.wpf.Extensions
{
    /// <summary>
    /// Class for any needed extensions
    /// </summary>
    public static class AppExtensions
    {
        /// <summary>
        /// /
        /// </summary>
        /// <param name="collection">The collection of IMimeParts, these should be attachments</param>
        /// <param name="id">the original source located</param>
        /// <param name="text">the text to update</param>
        /// <returns>The updated text</returns>
        public static string FindAndUpdate(this List<IMimeParts> collection, string id, string text, List<IMimeParts> removable)
        {
            foreach (IMimeParts part in collection)
            {
                if (part.ContentID != null)
                {
                    Match ContentMatch = Regex.Match(part.ContentID, "Content-ID: <(.+)>", RegexOptions.IgnoreCase);
                    Match typeMatch = Regex.Match(part.ContentType, "Content-Type: (.+);", RegexOptions.IgnoreCase);
                    if (ContentMatch.Success && typeMatch.Success && id.Contains(ContentMatch.Groups[1].Value))
                    {
                        string type = typeMatch.Groups[1].Value;
                        text = text.Replace(id, "data:"+type+";base64," + part.Contents.Replace(Environment.NewLine, String.Empty));
                        removable.Add(part);
                    }
                }
            }
            return text;
        }
    }
}

