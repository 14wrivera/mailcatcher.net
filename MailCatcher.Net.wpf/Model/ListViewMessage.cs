﻿using System.Collections.Generic;
using System.Windows.Documents;
using MailCatcher.Net.MailMessage.Interfaces;

namespace MailCatcher.Net.wpf.Model
{
    /// <summary>
    /// ListViewMessage with Additional Message Properties
    /// </summary>
    public class ListViewMessage : ListItem
    {
        #region Properties
        public string Message { get; set; }
        public string To { get; set; }
        public string From { get; set; }
        public string Subject { get; set; }
        public string Date { get; set; }

        public string HTMLMessage { get; set; }
        public string PlainMessage { get; set; }
        public List<IMimeParts> Attachments { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// Base Constructor
        /// </summary>
        /// <param name="message">The Overall Message</param>
        public ListViewMessage(List<string> message): base()
        {
            this.Message = string.Join( "\n", message);
            int len = 50;
            if (message.Count < 50)
            {
                len=message.Count -1;
            }
            for(int i=0; i<len;i++)
            {
                string msg = message[i];
                if (this.Subject == null && msg.StartsWith("Subject:"))
                    this.Subject = msg.Substring(8);
                if (this.Date == null && msg.StartsWith("Date:"))
                this.Date = msg.Substring(5);
            }
        }
        #endregion
    }
}
