﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security;

namespace MailCatcher.Net.Model
{
    /// <summary>
    /// Manages the Console window
    /// </summary>
    [SuppressUnmanagedCodeSecurity]
    public static class ConsoleManager
    {
        #region Private Static Members
        private const string Kernel32_DllName = "kernel32.dll";

        [DllImport(Kernel32_DllName)]
        private static extern bool AllocConsole();

        [DllImport(Kernel32_DllName)]
        private static extern bool FreeConsole();

        [DllImport(Kernel32_DllName)]
        private static extern IntPtr GetConsoleWindow();

        [DllImport(Kernel32_DllName)]
        private static extern int GetConsoleOutputCP();
        #endregion

        #region Static Properties
        public static bool HasConsole
        {
            get { return GetConsoleWindow() != IntPtr.Zero; }
        }
        #endregion

        #region Public Static Methods
        /// <summary>
        /// Creates a new console instance if the process is not attached to a console already.
        /// </summary>
        public static void Show()
        {
            if (!HasConsole)
            {
                AllocConsole();
                InvalidateOutAndError();
            }
        }

        /// <summary>
        /// If the process has a console attached to it, it will be detached and no longer visible. Writing to the System.Console is still possible, but no output will be shown.
        /// </summary>
        public static void Hide()
        {
            if (HasConsole)
            {
                SetOutAndErrorNull();
                FreeConsole();
            }
        }

        /// <summary>
        /// Toggle the console to hide or show
        /// </summary>
        public static void Toggle()
        {
            if (HasConsole)
            {
                Hide();
            }
            else
            {
                Show();
            }
        }
        #endregion

        #region Private Static Methods
        /// <summary>
        /// Bind errors
        /// </summary>
        private static void InvalidateOutAndError()
        {
            Type type = typeof(Console);

            FieldInfo _out = type.GetField("_out", BindingFlags.Static | BindingFlags.NonPublic);
            FieldInfo _error = type.GetField("_error", BindingFlags.Static | BindingFlags.NonPublic);
            MethodInfo _InitializeStdOutError = type.GetMethod("InitializeStdOutError",BindingFlags.Static | 
                BindingFlags.NonPublic);

            _out.SetValue(null, null);
            _error.SetValue(null, null);
            _InitializeStdOutError.Invoke(null, new object[] { true });
        }

        /// <summary>
        /// Sends out the error
        /// </summary>
        private static void SetOutAndErrorNull()
        {
            Console.SetOut(TextWriter.Null);
            Console.SetError(TextWriter.Null);
        }
        #endregion
    }
}
