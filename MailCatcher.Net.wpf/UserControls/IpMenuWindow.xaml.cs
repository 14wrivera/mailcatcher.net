﻿using System;
using System.Configuration;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using MailCatcher.Net.wpf.Interfaces;

namespace MailCatcher.Net.wpf.UserControls
{
    /// <summary>
    /// Interaction logic for IpMenuWindow.xaml
    /// </summary>
    public partial class IpMenuWindow : UserControl
    {
        public IApplicationWindow AppWindow { get; set; }

        public IpMenuWindow()
        {
            InitializeComponent();
            try
            {
                var ipAddress = ConfigurationManager.AppSettings["IP"].ToString();
                var port = ConfigurationManager.AppSettings["Port"].ToString();

                this.IPTxt.Text = ipAddress;
                this.PortTxt.Text = port;
            }
            catch(Exception exe)
            {
                Console.WriteLine("Exception: {0}", exe);
                this.AppWindow.ErrorNotification(String.Format("Exception: {0}", exe));
                this.AppWindow.StopServer();
            }
        }

        public void Open()
        {
            ((Storyboard)this.Resources["Expand"]).Begin();
        }

        /// <summary>
        /// update ip settings
        /// </summary>
        /// <param name="sender">Sending Object</param>
        /// <param name="e">Event Arguments</param>
        private void UpdateIP(object sender, EventArgs e)
        {
            try
            {
                this.AppWindow.StopServer();

                var ipAddress = this.IPTxt.Text;
                var port = int.Parse(this.PortTxt.Text);

                this.AppWindow.StartServer(IPAddress.Parse(ipAddress),port);
                
                var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.AppSettings.Settings["IP"].Value = ipAddress.ToString();
                config.AppSettings.Settings["Port"].Value = port.ToString();
                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");

            }
            catch (Exception exe)
            {
                Console.WriteLine("Exception: {0}", exe);
                this.AppWindow.ErrorNotification(String.Format("Exception: {0}", exe));
                this.AppWindow.StopServer();
            }
        }
    }
}
