﻿using System.Net;
using System.Windows.Controls.Primitives;

namespace MailCatcher.Net.wpf.Interfaces
{
    public interface IApplicationWindow
    {
        void ErrorNotification(string message);
        void StartServer();
        void StartServer(IPAddress address, int port);
        void StopServer();
    }
}
