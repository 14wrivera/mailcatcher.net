﻿using System;
using System.Linq;
using MailCatcher.Net.MailMessage;
using MailCatcher.Net.MailMessage.MimeTypes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MailCatcher.Net.Test.MailMessage
{
    [TestClass]
    public class MessagePartTest
    {
        private string message =
            "MIME-Version: 1.0\n" +
            "Content-Type: multipart/mixed; boundary=frontier\n\n" +
            "This is a message with multiple parts in MIME format.\n" +
            "--frontier\n" +
            "Content-Type: text/plain\n\n" +
            "This is the body of the message.\n" +
            "--frontier\n" +
            "Content-Type: application/octet-stream\n" +
            "Content-Transfer-Encoding: base64\n\n" +
            "PGh0bWw+CiAgPGhlYWQ+CiAgPC9oZWFkPgogIDxib2R5PgogICAgPHA+VGhpcyBpcyB0aGUg\n" +
            "Ym9keSBvZiB0aGUgbWVzc2FnZS48L3A+CiAgPC9ib2R5Pgo8L2h0bWw+Cg==\n" +
            "--frontier--";
        private string plain =
            "MIME-Version: 1.0\n" +
            "From: from@from.com\n" +
            "To: to@to.com\n" +
            "Date: 7 Jan 2013 09:34:13 -0500\n" +
            "Subject: Herro\n" +
            "Content-Type: text/plain; charset=us-ascii\n" +
            "Content-Transfer-Encoding: quoted-printable\n\n" +
            "This is the body of the message.\n\n";
        /// <summary>
        /// Tests a simple Message to ensure
        /// proper Parsing
        /// </summary>
        [TestMethod]
        public void TestParseSimple()
        {
            MessageParts parts = new MessageParts(plain);
            Assert.IsFalse(parts.IsMultiPart);
            Assert.AreEqual(1, parts.Parts.Count());
        }

        /// <summary>
        /// Tests a multi part Message to ensure
        /// proper Parsing
        /// </summary>
        [TestMethod]
        public void TestParseSimpleMultiPart()
        {
            MessageParts parts = new MessageParts(message);
            Assert.IsTrue(parts.IsMultiPart);
            Assert.AreEqual(2, parts.Parts.Count());
        }

        /// <summary>
        /// Test to make sure we can pull out parts by content type
        /// </summary>
        [TestMethod]
        public void FindByContentTypeTest()
        {
            MessageParts parts = new MessageParts(plain);
            MessagePart part = parts.FindByContentType("text/plain") as MessagePart;
            Assert.AreEqual("Content-Transfer-Encoding: quoted-printable",
                part.ContentTransferEncoding);
        }
    }
}
