﻿using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using MailCatcher.Net.Server.Interfaces;
using MailCatcher.Net.Server.SmtpServerResponse;
using MailCatcher.Net.Test.Fakes;

namespace MailCatcher.Net.Test.SmtpServerResponse
{
    /// <summary>
    /// Test The Response to the Data Message sent
    /// </summary>
    [TestClass]
    public class SmtpToResponseTest
    {
        /// <summary>
        /// Singleton test
        /// </summary>
        /// <param name="server"></param>
        /// <returns></returns>
        [TestMethod]
        private void ConstructorTest()
        {
            ISmtpServer server = new FakeSmtpServer();
            SmtpDataResponse response = SmtpDataResponse.RetrieveResponse(server);
            SmtpDataResponse response2 = SmtpDataResponse.RetrieveResponse(server);
            Assert.AreEqual(response, response2);
        }
    }
}
