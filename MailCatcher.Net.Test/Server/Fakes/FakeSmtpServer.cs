﻿using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using MailCatcher.Net.Server.Interfaces;
using MailCatcher.Net.Server;
using MailCatcher.Net.Server.EventArgs;

namespace MailCatcher.Net.Test.Fakes
{
    /// <summary>
    /// A fake SMTP server to test the Response creation
    /// </summary>
    public class FakeSmtpServer : ISmtpServer
    {
        public event EventHandler<ReadEventArgs> OnRead = null;

        public SmtpServerState State
        {
            get;
            set;
        }

        public SmtpServerMessage OutgoingMessage
        {
            get;
            set;
        }

        public void Run()
        {
            //do nothing
        }

        public void Close()
        {
            //do nothing
        }

        public void Write(string data)
        {
            //do nothing
        }

        public void Send(string data)
        {
            //do nothing
        }
    }
}
