﻿using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using MailCatcher.Net.Server;
using MailCatcher.Net.Server.Interfaces;
using MailCatcher.Net.Server.SmtpServerResponse;
using MailCatcher.Net.Test.Fakes;

namespace MailCatcher.Net.Test
{
    /// <summary>
    /// test SmtpMessage
    /// </summary>
    [TestClass]
    public class SmtpMessageTest
    {
        /// <summary>
        /// Ehlo Command Test
        /// </summary>
        [TestMethod]
        public void EhloTest()
        {
            SmtpServerMessage message = new SmtpServerMessage("EHLO", SmtpServerState.READING);
            Assert.IsTrue(message.Command == SmtpMessageCommands.NA);
            SmtpServerMessage message2 = new SmtpServerMessage("EHLO", SmtpServerState.WAITING);
            Assert.IsTrue(message2.Command == SmtpMessageCommands.EHLO);
            SmtpServerMessage message3 = new SmtpServerMessage("EHLO", SmtpServerState.CLOSE);
            Assert.IsTrue(message3.Command == SmtpMessageCommands.EHLO);
        }
        /// <summary>
        /// HELO command test
        /// </summary>
        [TestMethod]
        public void HeloTest()
        {
            SmtpServerMessage message = new SmtpServerMessage("HELO",SmtpServerState.READING);
            Assert.IsTrue(message.Command == SmtpMessageCommands.NA);
            SmtpServerMessage message2 = new SmtpServerMessage("HELO", SmtpServerState.WAITING);
            Assert.IsTrue(message2.Command == SmtpMessageCommands.EHLO);
            SmtpServerMessage message3 = new SmtpServerMessage("HELO", SmtpServerState.CLOSE);
            Assert.IsTrue(message3.Command == SmtpMessageCommands.EHLO);
        }
        /// <summary>
        /// Mail From command test
        /// </summary>
        [TestMethod]
        public void MailFromTest()
        {
            SmtpServerMessage message = new SmtpServerMessage("MAIL FROM", SmtpServerState.READING);
            Assert.IsTrue(message.Command == SmtpMessageCommands.NA);
            SmtpServerMessage message2 = new SmtpServerMessage("MAIL FROM", SmtpServerState.WAITING);
            Assert.IsTrue(message2.Command == SmtpMessageCommands.FROM);
            SmtpServerMessage message3 = new SmtpServerMessage("MAIL FROM", SmtpServerState.CLOSE);
            Assert.IsTrue(message3.Command == SmtpMessageCommands.FROM);
        }
        /// <summary>
        /// To command test
        /// </summary>
        [TestMethod]
        public void ToTest()
        {
            SmtpServerMessage message = new SmtpServerMessage("RCPT TO", SmtpServerState.READING);
            Assert.IsTrue(message.Command == SmtpMessageCommands.NA);
            SmtpServerMessage message2 = new SmtpServerMessage("RCPT TO", SmtpServerState.WAITING);
            Assert.IsTrue(message2.Command == SmtpMessageCommands.TO);
            SmtpServerMessage message3 = new SmtpServerMessage("RCPT TO", SmtpServerState.CLOSE);
            Assert.IsTrue(message3.Command == SmtpMessageCommands.TO);
        }

        /// <summary>
        /// Data command test
        /// </summary>
        [TestMethod]
        public void DataTest()
        {
            SmtpServerMessage message = new SmtpServerMessage("DATA ", SmtpServerState.READING);
            Assert.AreEqual(SmtpMessageCommands.NA, message.Command);
            SmtpServerMessage message2 = new SmtpServerMessage("DATA ", SmtpServerState.WAITING);
            Assert.AreEqual(SmtpMessageCommands.DATA, message2.Command);
            SmtpServerMessage message3 = new SmtpServerMessage("DATA ", SmtpServerState.CLOSE);
            Assert.AreEqual(SmtpMessageCommands.DATA, message3.Command);
        }

        /// <summary>
        /// EOF command test
        /// </summary>
        [TestMethod]
        public void EOFTest()
        {
            SmtpServerMessage message = new SmtpServerMessage(".", SmtpServerState.READING);
            Assert.IsTrue(message.Command == SmtpMessageCommands.EOF);
            SmtpServerMessage message2 = new SmtpServerMessage(".", SmtpServerState.WAITING);
            Assert.IsTrue(message2.Command == SmtpMessageCommands.EOF);
            SmtpServerMessage message3 = new SmtpServerMessage(".", SmtpServerState.CLOSE);
            Assert.IsTrue(message3.Command == SmtpMessageCommands.EOF);
        }

        /// <summary>
        /// Quit command test
        /// </summary>
        [TestMethod]
        public void QuitTest()
        {
            SmtpServerMessage message = new SmtpServerMessage("QUIT", SmtpServerState.READING);
            Assert.IsTrue(message.Command == SmtpMessageCommands.NA);
            SmtpServerMessage message2 = new SmtpServerMessage("QUIT", SmtpServerState.WAITING);
            Assert.IsTrue(message2.Command == SmtpMessageCommands.QUIT);
            SmtpServerMessage message3 = new SmtpServerMessage("QUIT", SmtpServerState.CLOSE);
            Assert.IsTrue(message3.Command == SmtpMessageCommands.QUIT);
        }

        /// <summary>
        /// Unsupported command test
        /// </summary>
        [TestMethod]
        public void NATest()
        {
            SmtpServerMessage message = new SmtpServerMessage("SOMEthing else", SmtpServerState.READING);
            Assert.IsTrue(message.Command == SmtpMessageCommands.NA);
            SmtpServerMessage message2 = new SmtpServerMessage("SOMEthing else", SmtpServerState.WAITING);
            Assert.IsTrue(message2.Command == SmtpMessageCommands.NA);
            SmtpServerMessage message3 = new SmtpServerMessage("SOMEthing else", SmtpServerState.CLOSE);
            Assert.IsTrue(message3.Command == SmtpMessageCommands.NA);
        }
    }
}
