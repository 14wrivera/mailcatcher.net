﻿using System;
using System.Net;
using System.Net.Sockets;
using MailCatcher.Net.Server;
using MailCatcher.Net.Server.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace MailCatcher.Net.Test.Server
{
    [TestClass]
    public class SmtpServerTest
    {
        [TestInitialize] 
        public void start()
        {
            tcp = new TcpListener(IPAddress.Parse("127.0.0.1"),13000);
        }

        TcpListener tcp;

        [TestMethod]
        [ExpectedException(typeof(SocketException))]
        public void TcpException()
        {
            tcp.Start();

            MailCatcherServer server = new MailCatcherServer();
            server.Start();
        }

        [TestCleanup]
        public void TearDown()
        {
            tcp.Stop();
        }
    }
}
