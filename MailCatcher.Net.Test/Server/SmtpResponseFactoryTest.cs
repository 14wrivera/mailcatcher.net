﻿using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using MailCatcher.Net.Server;
using MailCatcher.Net.Server.Interfaces;
using MailCatcher.Net.Server.SmtpServerResponse;
using MailCatcher.Net.Test.Fakes;

namespace MailCatcher.Net.Test
{
    /// <summary>
    /// Test for smtpResponseFactory
    /// </summary>
    [TestClass]
    public class SmtpResponseFactoryTest
    {
        /// <summary>
        /// Test the retrieval of the EHLO response
        /// </summary>
        [TestMethod]
        public void RetrieveEhloResponseTest()
        {
            SmtpResponseFactory factory = new SmtpResponseFactory();
            factory.Server = new FakeSmtpServer(); //wait close

            factory.Server.State = SmtpServerState.READING;
            factory.Server.OutgoingMessage = new SmtpServerMessage("EHLO", factory.Server.State);
            ISmtpResponse response = factory.Retrieve();
            Assert.IsTrue(response is SmtpReadResponse);

            factory.Server.State = SmtpServerState.WAITING;
            factory.Server.OutgoingMessage = new SmtpServerMessage("EHLO", factory.Server.State);
            ISmtpResponse response2 = factory.Retrieve();
            Assert.IsTrue(response2 is SmtpEHLOResponse);

            factory.Server.State = SmtpServerState.CLOSE;
            factory.Server.OutgoingMessage = new SmtpServerMessage("EHLO", factory.Server.State);
            ISmtpResponse response3 = factory.Retrieve();
            Assert.IsTrue(response3 is SmtpEHLOResponse);
        }

        /// <summary>
        /// Test the retrieval of the HELO response
        /// </summary>
        [TestMethod]
        public void RetrieveHeloResponseTest()
        {
            SmtpResponseFactory factory = new SmtpResponseFactory();
            factory.Server = new FakeSmtpServer(); //wait close

            factory.Server.State = SmtpServerState.READING;
            factory.Server.OutgoingMessage = new SmtpServerMessage("HELO", factory.Server.State);
            ISmtpResponse response = factory.Retrieve();
            Assert.IsTrue(response is SmtpReadResponse);

            factory.Server.State = SmtpServerState.WAITING;
            factory.Server.OutgoingMessage = new SmtpServerMessage("HELO", factory.Server.State);
            ISmtpResponse response2 = factory.Retrieve();
            Assert.IsTrue(response2 is SmtpEHLOResponse);

            factory.Server.State = SmtpServerState.CLOSE;
            factory.Server.OutgoingMessage = new SmtpServerMessage("HELO", factory.Server.State);
            ISmtpResponse response3 = factory.Retrieve();
            Assert.IsTrue(response3 is SmtpEHLOResponse);
        }

        /// <summary>
        /// Test the retrieval of the Quit response
        /// </summary>
        [TestMethod]
        public void RetrieveQuitResponseTest()
        {
            SmtpResponseFactory factory = new SmtpResponseFactory();
            factory.Server = new FakeSmtpServer(); //wait close

            factory.Server.State = SmtpServerState.READING;
            factory.Server.OutgoingMessage = new SmtpServerMessage("QUIT", factory.Server.State);
            ISmtpResponse response = factory.Retrieve();
            Assert.IsTrue(response is SmtpReadResponse);

            factory.Server.State = SmtpServerState.WAITING;
            factory.Server.OutgoingMessage = new SmtpServerMessage("QUIT", factory.Server.State);
            ISmtpResponse response2 = factory.Retrieve();
            Assert.IsTrue(response2 is SmtpQuitResponse);

            factory.Server.State = SmtpServerState.CLOSE;
            factory.Server.OutgoingMessage = new SmtpServerMessage("QUIT", factory.Server.State);
            ISmtpResponse response3 = factory.Retrieve();
            Assert.IsTrue(response3 is SmtpQuitResponse);
        }

        /// <summary>
        /// Test the retrieval of the To response
        /// </summary>
        [TestMethod]
        public void RetrieveToResponseTest()
        {
            SmtpResponseFactory factory = new SmtpResponseFactory();
            factory.Server = new FakeSmtpServer(); //wait close

            factory.Server.State = SmtpServerState.READING;
            factory.Server.OutgoingMessage = new SmtpServerMessage("RCPT TO", factory.Server.State);
            ISmtpResponse response = factory.Retrieve();
            Assert.IsTrue(response is SmtpReadResponse);

            factory.Server.State = SmtpServerState.WAITING;
            factory.Server.OutgoingMessage = new SmtpServerMessage("RCPT TO", factory.Server.State);
            ISmtpResponse response2 = factory.Retrieve();
            Assert.IsTrue(response2 is SmtpToResponse);

            factory.Server.State = SmtpServerState.CLOSE;
            factory.Server.OutgoingMessage = new SmtpServerMessage("RCPT TO", factory.Server.State);
            ISmtpResponse response3 = factory.Retrieve();
            Assert.IsTrue(response3 is SmtpToResponse);
        }

        /// <summary>
        /// Test the retrieval of the From response
        /// </summary>
        [TestMethod]
        public void RetrieveFromResponseTest()
        {
            SmtpResponseFactory factory = new SmtpResponseFactory();
            factory.Server = new FakeSmtpServer(); //wait close

            factory.Server.State = SmtpServerState.READING;
            factory.Server.OutgoingMessage = new SmtpServerMessage("MAIL FROM", factory.Server.State);
            ISmtpResponse response = factory.Retrieve();
            Assert.IsTrue(response is SmtpReadResponse);

            factory.Server.State = SmtpServerState.WAITING;
            factory.Server.OutgoingMessage = new SmtpServerMessage("MAIL FROM", factory.Server.State);
            ISmtpResponse response2 = factory.Retrieve();
            Assert.IsTrue(response2 is SmtpFromResponse);

            factory.Server.State = SmtpServerState.CLOSE;
            factory.Server.OutgoingMessage = new SmtpServerMessage("MAIL FROM", factory.Server.State);
            ISmtpResponse response3 = factory.Retrieve();
            Assert.IsTrue(response3 is SmtpFromResponse);
        }

        /// <summary>
        /// Test the retrieval of the EOF response
        /// </summary>
        [TestMethod]
        public void RetrieveEOFResponseTest()
        {
            SmtpResponseFactory factory = new SmtpResponseFactory();
            factory.Server = new FakeSmtpServer(); //wait close

            factory.Server.State = SmtpServerState.READING;
            factory.Server.OutgoingMessage = new SmtpServerMessage(".", factory.Server.State);
            ISmtpResponse response = factory.Retrieve();
            Assert.IsTrue(response is SmtpEOFResponse);

            factory.Server.State = SmtpServerState.WAITING;
            factory.Server.OutgoingMessage = new SmtpServerMessage(".", factory.Server.State);
            ISmtpResponse response2 = factory.Retrieve();
            Assert.IsTrue(response2 is SmtpEOFResponse);

            factory.Server.State = SmtpServerState.CLOSE;
            factory.Server.OutgoingMessage = new SmtpServerMessage(".", factory.Server.State);
            ISmtpResponse response3 = factory.Retrieve();
            Assert.IsTrue(response3 is SmtpEOFResponse);
        }

        /// <summary>
        /// Test the retrieval of the DATA response
        /// </summary>
        [TestMethod]
        public void RetrieveDATAResponseTest()
        {
            SmtpResponseFactory factory = new SmtpResponseFactory();
            factory.Server = new FakeSmtpServer(); //wait close

            factory.Server.State = SmtpServerState.READING;
            factory.Server.OutgoingMessage = new SmtpServerMessage("DATA", factory.Server.State);
            ISmtpResponse response = factory.Retrieve();
            Assert.IsTrue(response is SmtpReadResponse);

            factory.Server.State = SmtpServerState.WAITING;
            factory.Server.OutgoingMessage = new SmtpServerMessage("DATA", factory.Server.State);
            ISmtpResponse response2 = factory.Retrieve();
            Assert.IsTrue(response2 is SmtpDataResponse);

            factory.Server.State = SmtpServerState.CLOSE;
            factory.Server.OutgoingMessage = new SmtpServerMessage("DATA", factory.Server.State);
            ISmtpResponse response3 = factory.Retrieve();
            Assert.IsTrue(response3 is SmtpDataResponse);
        }

        /// <summary>
        /// Test the retrieval of the NA response
        /// </summary>
        [TestMethod]
        public void RetrieveANResponseTest()
        {
            SmtpResponseFactory factory = new SmtpResponseFactory();
            factory.Server = new FakeSmtpServer(); //wait close

            factory.Server.State = SmtpServerState.READING;
            factory.Server.OutgoingMessage = new SmtpServerMessage("anything", factory.Server.State);
            ISmtpResponse response = factory.Retrieve();
            Assert.IsTrue(response is SmtpReadResponse);

            factory.Server.State = SmtpServerState.WAITING;
            factory.Server.OutgoingMessage = new SmtpServerMessage("anything", factory.Server.State);
            ISmtpResponse response2 = factory.Retrieve();
            Assert.IsTrue(response2 is SmtpNAResponse);

            factory.Server.State = SmtpServerState.CLOSE;
            factory.Server.OutgoingMessage = new SmtpServerMessage("anything", factory.Server.State);
            ISmtpResponse response3 = factory.Retrieve();
            Assert.IsTrue(response3 is SmtpNAResponse);
        }
    }
}
