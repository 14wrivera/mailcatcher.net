﻿using MailCatcher.Net.Server.Interfaces;
using MailCatcher.Net.Server.SmtpServerResponse;

namespace MailCatcher.Net.Server
{
    /// <summary>
    /// The implemented Response Factory
    /// </summary>
    public class SmtpResponseFactory : ISmtpResponseFactory
    {
        public ISmtpServer Server { get; set; }
        /// <summary>
        /// Retrieve the response based on the message
        /// </summary>
        /// <returns>the Response</returns>
        public ISmtpResponse Retrieve()
        {
            if (this.Server.State == SmtpServerState.READING
                && this.Server.OutgoingMessage.Command != SmtpMessageCommands.EOF)
            {
                return SmtpReadResponse.RetrieveResponse(this.Server);
            }
            //Map the Response from the Code if not in Read State
            switch (this.Server.OutgoingMessage.Command)
            {
                case SmtpMessageCommands.EHLO:
                    return SmtpEHLOResponse.RetrieveResponse(this.Server, this.Server.OutgoingMessage.FullText);
                case SmtpMessageCommands.QUIT:
                    return SmtpQuitResponse.RetrieveResponse(this.Server);
                case SmtpMessageCommands.TO:
                    return SmtpToResponse.RetrieveResponse(this.Server);
                case SmtpMessageCommands.FROM:
                    return SmtpFromResponse.RetrieveResponse(this.Server);
                case SmtpMessageCommands.EOF:
                    return SmtpEOFResponse.RetrieveResponse(this.Server);
                case SmtpMessageCommands.DATA:
                    return SmtpDataResponse.RetrieveResponse(this.Server);
                case SmtpMessageCommands.READING:
                    return SmtpReadResponse.RetrieveResponse(this.Server);
            }
            return SmtpNAResponse.RetrieveResponse(this.Server);
        }
    }
}
