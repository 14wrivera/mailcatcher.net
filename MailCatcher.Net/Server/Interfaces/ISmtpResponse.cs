﻿
namespace MailCatcher.Net.Server.Interfaces
{
    /// <summary>
    /// Base Response Type
    /// </summary>
    public interface ISmtpResponse
    {
        ISmtpServer Server { get; set; }
        SmtpMessageCommands Code { get; set; }
        void Respond(string data);
    }
}