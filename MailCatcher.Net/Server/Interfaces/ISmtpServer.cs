﻿using System;
using MailCatcher.Net.Server.EventArgs;

namespace MailCatcher.Net.Server.Interfaces
{
    /// <summary>
    /// Defines SmtpServer Behaviors
    /// </summary>
    public interface ISmtpServer
    {
        event EventHandler<ReadEventArgs> OnRead;
        SmtpServerMessage OutgoingMessage { get; set; }
        SmtpServerState State { get; set; }
        void Run();
        void Close();
        void Write(string data);
        void Send(string data);
    }
}