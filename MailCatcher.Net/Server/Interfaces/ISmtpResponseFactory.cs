﻿
namespace MailCatcher.Net.Server.Interfaces
{
    /// <summary>
    /// This Generates SMTP messages
    /// </summary>
    public interface ISmtpResponseFactory
    {
        /// <summary>
        /// Retrieve the response based on the message
        /// </summary>
        /// <returns>the Response</returns>
        ISmtpResponse Retrieve();
        ISmtpServer Server { get; set; }
    }
}