﻿using System;
using System.IO;
using System.Net.Sockets;
using MailCatcher.Net.Server.EventArgs;
using MailCatcher.Net.Server.Interfaces;

namespace MailCatcher.Net.Server
{
    /// <summary>
    /// SmtpServer for processing Smtp Messages
    /// This is a simple implementation with limited functionality
    /// </summary>
    public class SmtpServer : ISmtpServer
    {
        #region Properties
        public TcpClient Client
        {
            set
            {
                this._client = value;
                // Get a stream object for reading and writing
                this._stream = this._client.GetStream();
                this._stream.ReadTimeout = 1000;
            }
        }
        public SmtpServerMessage OutgoingMessage { get; set; }
        public ISmtpResponse ResponseType { get; set; }
        public ISmtpResponseFactory ResponseTypeFactory { get; set; }
        public SmtpServerState State { get; set; }
        #endregion

        #region Members
        private TcpClient _client;
        private NetworkStream _stream;
        #endregion

        #region Events
        public event EventHandler<ReadEventArgs> OnRead;
        #endregion

        #region Constructor
        /// <summary>
        /// The SmtpServer Constructor which needs a
        /// Message Factory to work properly
        /// </summary>
        /// <param name="factory">is a <typeparamref name="ISmtpResponseFactory"/>THe Factory for 
        /// SMTP Response Creation</param>
        public SmtpServer(ISmtpResponseFactory factory)
        {
            this.ResponseTypeFactory = factory;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Reads the Streams While the connection is open
        /// </summary>
        public void Run()
        {
            this.State = SmtpServerState.WAITING;
            string data = null;
            StreamReader input = new StreamReader(this._stream);
            try
            {
                // Loop to receive all the data sent by the client.
                while (this.State != SmtpServerState.CLOSE &&
                    ((data = input.ReadLine()) != null)
                )
                {
                    this.Process(data);
                }
            }
            catch (IOException exc)
            {
                this.Write(exc.Message);
            }
        }

        /// <summary>
        /// Closes the Socket
        /// </summary>
        public void Close()
        {
            if (this._client !=null)
            {
                this._client.Close();
            }
        }

        /// <summary>
        /// Reads the incoming data
        /// </summary>
        /// <param name="data"></param>
        public void Write(string data)
        {
            Console.WriteLine(data);
        }

        /// <summary>
        /// Sends Data to the Client
        /// </summary>
        /// <param name="data"></param>
        public void Send(string data)
        {
            data = data.ToUpper();
            // Send back a response.
            StreamWriter output = new StreamWriter(this._stream);
            output.WriteLine(data.ToUpper());
            output.Flush();

            this.Write(String.Format("Sent: {0}", data));
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Thread safe event
        /// </summary>
        /// <param name="data">the data</param>
        /// <param name="state">the SmtpServerState</param>
        /// <param name="command">the Message command</param>
        protected virtual void Read(string data, SmtpServerState state, SmtpMessageCommands command)
        {
            if (this.OnRead != null)
                this.OnRead(this, new ReadEventArgs() { Data = data, State = state, Command = command });
        }
        /// <summary>
        /// Process data
        /// </summary>
        /// <param name="data">The Data at it comes in</param>
        private void Process(string data)
        {
            //Parse Message
            this.OutgoingMessage = new SmtpServerMessage(data, this.State);
            //set the response type and pass in the 
            this.ResponseType = this.ResponseTypeFactory.Retrieve();
            this.ResponseType.Respond(data);
            //Fire Event
            this.Read(data,this.State,this.OutgoingMessage.Command);
        }
        #endregion
    }
    /// <summary>
    /// The Enum with possible ServerStates
    /// </summary>
    public enum SmtpServerState : int
    {
        READING = 1,
        WAITING = 2,
        CLOSE = 3
    }
}
