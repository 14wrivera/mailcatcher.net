﻿
using MailCatcher.Net.Server.Interfaces;

namespace MailCatcher.Net.Server.SmtpServerResponse
{
    /// <summary>
    /// The initial Hello Response
    /// </summary>
    public class SmtpEHLOResponse : ISmtpResponse
    {
        #region Static Members
        private static volatile SmtpEHLOResponse instance;
        #endregion

        #region Public Static Method
        /// <summary>
        /// retrieves Singleton instance of this Class
        /// </summary>
        /// <param name="server">is a <typeparamref name="ISmtpServer"/>. The Server</param>
        /// <returns>ISmtpResponse Instance</returns>
        public static SmtpEHLOResponse RetrieveResponse(ISmtpServer server, string message)
        {
            if (SmtpEHLOResponse.instance == null)
            {
                lock (new object())
                {
                    if (SmtpEHLOResponse.instance == null)
                        SmtpEHLOResponse.instance = new SmtpEHLOResponse(server, message);
                }
            }
                
            return SmtpEHLOResponse.instance;
        }
        #endregion

        #region Properties
        public ISmtpServer Server { get; set; }
        public SmtpMessageCommands Code { get; set; }
        public string Message { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor with Reference to the Server
        /// </summary>
        /// <param name="server">is a <typeparamref name="ISmtpServer"/>. The Server</param>
        /// <param name="message">the message to append</param>
        private SmtpEHLOResponse(ISmtpServer server, string message)
        {
            this.Server = server;
            this.Code = SmtpMessageCommands.EHLO;
            this.Message = "250 Hello " + message.Substring(4) + ", I am glad to meet you";
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Respond to the message
        /// </summary>
        /// <param name="data">the incoming data</param>
        public void Respond(string data)
        {
            //implement respond action
            this.Server.Write(data);
            this.Server.Send(this.Message);
            this.Server.State = SmtpServerState.WAITING;
        }
        #endregion
    }
}
