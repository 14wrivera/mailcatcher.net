﻿
using MailCatcher.Net.Server.Interfaces;

namespace MailCatcher.Net.Server.SmtpServerResponse
{
    /// <summary>
    /// The Quit Response
    /// </summary>
    public class SmtpQuitResponse : ISmtpResponse
    {
        #region Static Members
        private static volatile SmtpQuitResponse instance;
        #endregion

        #region Public Static Method
        /// <summary>
        /// retrieves Singleton instance of this Class
        /// </summary>
        /// <param name="server">is a <typeparamref name="ISmtpServer"/>. The Server</param>
        /// <returns>ISmtpResponse Instance</returns>
        public static SmtpQuitResponse RetrieveResponse(ISmtpServer server)
        {
            if (instance == null)
            {
                lock (new object())
                {
                    if (instance == null)
                        instance = new SmtpQuitResponse(server);
                }
            }

            return SmtpQuitResponse.instance;
        }
        #endregion


        #region Properties
        public ISmtpServer Server { get; set; }
        public SmtpMessageCommands Code { get; set; }
        public string Message { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor with Reference to the Server
        /// </summary>
        /// <param name="server">is a <typeparamref name="ISmtpServer"/>. The Server</param>
        private SmtpQuitResponse(ISmtpServer server)
        {
            this.Server = server;
            this.Code = SmtpMessageCommands.QUIT;
            this.Message = "221 BYE";
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Respond to the message
        /// </summary>
        /// <param name="data">the incoming data</param>
        public void Respond(string data)
        {
            //implement respond action
            this.Server.Write(data);
            this.Server.Send(this.Message);
            this.Server.State = SmtpServerState.CLOSE;
        }
        #endregion
    }
}
