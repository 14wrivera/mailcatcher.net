﻿
using MailCatcher.Net.Server.Interfaces;

namespace MailCatcher.Net.Server.SmtpServerResponse
{
    /// <summary>
    /// The NA response. Not Implementing all commands
    /// will call this Response and quit the connection
    /// </summary>
    public class SmtpNAResponse : ISmtpResponse
    {
        #region Static Members
        private static volatile SmtpNAResponse instance;
        #endregion

        #region Public Static Method
        /// <summary>
        /// retrieves Singleton instance of this Class
        /// </summary>
        /// <param name="server">is a <typeparamref name="ISmtpServer"/>. The Server</param>
        /// <returns>ISmtpResponse Instance</returns>
        public static SmtpNAResponse RetrieveResponse(ISmtpServer server)
        {
            if (instance == null)
            {
                lock (new object())
                {
                    if (instance == null)
                        instance = new SmtpNAResponse(server);
                }
            }

            return SmtpNAResponse.instance;
        }
        #endregion

        #region Properties
        public ISmtpServer Server { get; set; }
        public SmtpMessageCommands Code { get; set; }
        public string Message { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor with Reference to the Server
        /// </summary>
        /// <param name="server">is a <typeparamref name="ISmtpServer"/>. The Server</param>
        private SmtpNAResponse(ISmtpServer server)
        {
            this.Server = server;
            this.Code = SmtpMessageCommands.NA;
            this.Message = "502 SORRY I AM NOT THAT SMART, I AM NOT SURE WHAT YOU WANT";
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Respond to the message
        /// </summary>
        /// <param name="data">the incoming data</param>
        public void Respond(string data)
        {
            //implement respond action
            this.Server.Write(data);
            this.Server.Send(this.Message);
            this.Server.State = SmtpServerState.WAITING;
        }
        #endregion
    }
}