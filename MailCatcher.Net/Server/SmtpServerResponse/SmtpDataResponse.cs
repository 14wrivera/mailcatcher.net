﻿
using MailCatcher.Net.Server.Interfaces;

namespace MailCatcher.Net.Server.SmtpServerResponse
{
    /// <summary>
    /// The Response to the Data Message sent
    /// </summary>
    public class SmtpDataResponse : ISmtpResponse
    {
        #region Static Member
        private static volatile SmtpDataResponse instance;
        #endregion

        #region Public Static Method
        /// <summary>
        /// retrieves Singleton instance of this Class
        /// </summary>
        /// <param name="server">is a <typeparamref name="ISmtpServer"/>. The Server</param>
        /// <returns>ISmtpResponse Instance</returns>
        public static SmtpDataResponse RetrieveResponse(ISmtpServer server)
        {
            if (instance == null)
            {
                lock (new object())
                {
                    if (instance == null)
                        instance = new SmtpDataResponse(server);
                }
            }

            return SmtpDataResponse.instance;
        }
        #endregion

        #region Properties
        public ISmtpServer Server { get; set; }
        public SmtpMessageCommands Code { get; set; }
        public string Message { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor with Reference to the Server
        /// </summary>
        /// <param name="server">is a <typeparamref name="ISmtpServer"/>. The Server</param>
        private SmtpDataResponse(ISmtpServer server)
        {
            this.Server = server;
            this.Code = SmtpMessageCommands.DATA;
            this.Message = "354 End data with <CR><LF>.<CR><LF>";
        }
        #endregion

        #region Public Method
        /// <summary>
        /// Respond to the message
        /// </summary>
        /// <param name="data">the incoming data</param>
        public void Respond(string data)
        {
            //implement respond action
            this.Server.Write(data);
            this.Server.Send(this.Message);
            this.Server.State = SmtpServerState.READING;
        }
        #endregion
    }
}
