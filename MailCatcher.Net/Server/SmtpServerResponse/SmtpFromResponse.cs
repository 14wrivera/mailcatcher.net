﻿using System;

using MailCatcher.Net.Server.Interfaces;

namespace MailCatcher.Net.Server.SmtpServerResponse
{
    /// <summary>
    /// The From Response
    /// </summary>
    public class SmtpFromResponse : ISmtpResponse
    {
        #region Static Members
        private static volatile SmtpFromResponse instance;
        #endregion

        #region Public Static Method
        /// <summary>
        /// retrieves Singleton instance of this Class
        /// </summary>
        /// <param name="server">is a <typeparamref name="ISmtpServer"/>. The Server</param>
        /// <returns>ISmtpResponse Instance</returns>
        public static SmtpFromResponse RetrieveResponse(ISmtpServer server)
        {
            if (instance == null)
            {
                lock (new object())
                {
                    if (instance == null)
                        instance = new SmtpFromResponse(server);
                }
            }

            return SmtpFromResponse.instance;
        }
        #endregion

        #region Properties
        public ISmtpServer Server { get; set; }
        public SmtpMessageCommands Code { get; set; }
        public string Message { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor with Reference to the Server
        /// </summary>
        /// <param name="server">is a <typeparamref name="ISmtpServer"/>. The Server</param>
        private SmtpFromResponse(ISmtpServer server)
        {
            this.Server = server;
            this.Code = SmtpMessageCommands.FROM;
            this.Message = "250 OK";
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Respond to the message
        /// </summary>
        /// <param name="data">the incoming data</param>
        public void Respond(string data)
        {
            //implement respond action
            this.Server.Write(data);
            this.Server.Send(this.Message);
            this.Server.State = SmtpServerState.WAITING;
        }
        #endregion
    }
}