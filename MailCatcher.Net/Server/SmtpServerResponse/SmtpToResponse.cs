﻿
using MailCatcher.Net.Server.Interfaces;

namespace MailCatcher.Net.Server.SmtpServerResponse
{
    /// <summary>
    /// The Smtp To Response
    /// </summary>
    public class SmtpToResponse : ISmtpResponse
    {
        #region Static Members
        private static volatile SmtpToResponse instance;
        #endregion

        #region Public Static Method
        /// <summary>
        /// retrieves Singleton instance of this Class
        /// </summary>
        /// <param name="server">is a <typeparamref name="ISmtpServer"/>. The Server</param>
        /// <returns>ISmtpResponse Instance</returns>
        public static SmtpToResponse RetrieveResponse(ISmtpServer server)
        {
            if (instance == null)
            {
                lock (new object())
                {
                    if (instance == null)
                        instance = new SmtpToResponse(server);
                }
            }
            return SmtpToResponse.instance;
        }
        #endregion


        #region Properties
        public ISmtpServer Server { get; set; }
        public SmtpMessageCommands Code { get; set; }
        public string Message { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor with Reference to the Server
        /// </summary>
        /// <param name="server">is a <typeparamref name="ISmtpServer"/>. The Server</param>
        private SmtpToResponse(ISmtpServer server)
        {
            this.Server = server;
            this.Code = SmtpMessageCommands.TO;
            this.Message = "250 OK";
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Respond to the message
        /// </summary>
        /// <param name="data">the incoming data</param>
        public void Respond(string data)
        {
            //implement respond action
            this.Server.Write(data);
            this.Server.Send(this.Message);
            this.Server.State = SmtpServerState.WAITING;
        }
        #endregion
    }
}
