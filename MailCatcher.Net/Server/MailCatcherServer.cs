﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using MailCatcher.Net.Server.EventArgs;
using MailCatcher.Net.Server.Interfaces;

namespace MailCatcher.Net.Server
{
    /// <summary>
    /// This is a wrapper class which starts the
    /// entire process
    /// </summary>
    public class MailCatcherServer
    {
        #region Members
        private bool _isStarted;
        private SmtpServer _smtpServer;
        private SmartTcpListener _tcpListener;
        private CancellationTokenSource _tokenSource;
        private List<String> _lastRecievedMessage;
        private List<String> _lastRecievedMessageHeader;
        #endregion

        #region Properties
        public Task Thread { get; set; }
        public IPAddress IpAddress{ get; set; }
        public Int32 Port { get; set; }
        #endregion

        #region Events
        public event EventHandler OnMessageStart;
        public event EventHandler<ReadEventArgs> OnMessageComplete;
        public event EventHandler<ReadEventArgs> OnRead;
        public event EventHandler<MessageEventArgs> OnMessageReceived;
        #endregion

        #region Constructor
        /// <summary>
        /// THis constructor allows for Address changes
        /// </summary>
        /// <param name="localAddr">is a <typeparamref name="IPAddress"/>the IP address</param>
        /// <param name="port">The port</param>
        public MailCatcherServer(IPAddress localAddr, Int32 port)
        {
            this.IpAddress = localAddr;
            this.Port = port;

            ISmtpResponseFactory factory = new SmtpResponseFactory();
            this._smtpServer = new SmtpServer(factory);
            factory.Server = this._smtpServer;
            //Bubble up onReadEvent
            this._smtpServer.OnRead += this.ReadData;
        }
        /// <summary>
        /// The Mail Catcher Service Constructor which uses the default ip and port
        /// </summary>
        public MailCatcherServer():this(IPAddress.Parse("127.0.0.1"), 13000){ }
        #endregion

        #region Public Methods
        /// <summary>
        /// Stop the server 
        /// </summary>
        /// <remarks> Essentially we Set the TcpListener to cancel, 
        /// cancel the task, set the while loop to false and close the Tcp client off
        /// if it was reading</remarks>
        public void Stop()
        {
            this._smtpServer.Close();
            this._isStarted = false;
            this._tcpListener.Stop();
            this._tokenSource.Cancel();
        }
        
        /// <summary>
        /// Starts the server thread
        /// </summary>
        public void Start()
        {
            //set up thread
            _tokenSource = new CancellationTokenSource();
            CancellationToken token = _tokenSource.Token;
            this._tcpListener = new SmartTcpListener(this.IpAddress, this.Port);
            if (_tcpListener.IsEndPointListenedTo())
                throw new SocketException(10048);
            this.Thread = Task.Factory.StartNew(ThreadAction, token);
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Thread Implementation
        /// </summary>
        private void ThreadAction()
        {
            this._isStarted = true;
            try
            {
                this._tcpListener.Start();
                Run();
            }
            catch (SocketException e)
            {
                if (e.ErrorCode != 10004)
                    throw;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Thread safe event
        /// </summary>
        /// <param name="args">read event params</param>
        protected virtual void Read(ReadEventArgs args)
        {
            if (this.OnRead != null)
                this.OnRead(this, args);
        }

        /// <summary>
        /// Thread safe event
        /// </summary>
        /// <param name="args">read event params</param>
        protected virtual void MessageReceived(MessageEventArgs args)
        {
            if (this.OnMessageReceived != null)
                this.OnMessageReceived(this, args);
        }


        /// <summary>
        /// Thread safe event
        /// </summary>
        /// <param name="args">read event params</param>
        protected virtual void MessageComplete(ReadEventArgs args)
        {
            if (this.OnMessageComplete != null)
                this.OnMessageComplete(this, args);
        }

        /// <summary>
        /// Thread safe event
        /// </summary>
        /// <param name="args">read event params</param>
        protected virtual void MessageStart()
        {
            if (this.OnMessageStart != null)
                this.OnMessageStart(this, new System.EventArgs() { });
        }
        

        /// <summary>
        /// Read Event
        /// </summary>
        /// <param name="data">The data that is read</param>
        /// <param name="serverState">THe current State</param>
        private void ReadData(Object sender, ReadEventArgs args)
        {
            this.Read(args);

            if (args.Command != SmtpMessageCommands.DATA &&
               args.State == SmtpServerState.READING)
            {
                this._lastRecievedMessage.Add(args.Data);
            }
            else if (args.Command == SmtpMessageCommands.FROM ||
               args.Command == SmtpMessageCommands.TO
               )
            {
                this._lastRecievedMessageHeader.Add(args.Data);
            }
        }

        /// <summary>
        /// Start Read Event
        /// </summary>
        /// <param name="data">The data that is going to be read</param>
        private void StartRead(string data)
        {
            this.MessageStart();
            this._lastRecievedMessage = new List<string>();
            this._lastRecievedMessageHeader = new List<string>();
        }

        /// <summary>
        /// End Read Event
        /// </summary>
        /// <param name="data">The data that is finished read</param>
        private void FinishRead(string data)
        {
            this.MessageComplete(new ReadEventArgs() { Data = data, State = this._smtpServer.State, Command = SmtpMessageCommands.EHLO });
            this.MessageReceived(new MessageEventArgs() { Message = this._lastRecievedMessage, Headers = this._lastRecievedMessageHeader });
        }
        /// <summary>
        /// Run the server And send the initial Response
        /// </summary>
        private void Run()
        {
            // Enter the listening loop.
            while (this._isStarted == true)
            {
                this._smtpServer.Write("Waiting for a connection... ");
                // Perform a blocking call to accept requests.
                this._smtpServer.Client = _tcpListener.AcceptTcpClient();
                this._smtpServer.Write("Connected!");
                this._smtpServer.OutgoingMessage = new SmtpServerMessage
                {
                    Command = SmtpMessageCommands.BROADCAST 
                };
                this._smtpServer.Send("220 MailCatcher.Net SMTP service ready");
                this.StartRead("Start");
                this._smtpServer.Run();
                this.FinishRead("END");
                // Shutdown and end connection
                this._smtpServer.Close();
            }
        }
        #endregion
    }
}
