﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace MailCatcher.Net.Server
{
    public class SmartTcpListener: TcpListener
    {
        #region Properties
        public new bool Active
        {
            get { return base.Active; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Net.Sockets.TcpListener"/> class with the specified local endpoint.
        /// </summary>
        /// <param name="localEP">An <see cref="T:System.Net.IPEndPoint"/> that represents the local endpoint to which to bind the listener <see cref="T:System.Net.Sockets.Socket"/>. </param><exception cref="T:System.ArgumentNullException"><paramref name="localEP"/> is null. </exception>
        public SmartTcpListener(IPEndPoint localEP)
            : base(localEP)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Net.Sockets.TcpListener"/> class that listens for incoming connection attempts on the specified local IP address and port number.
        /// </summary>
        /// <param name="localaddr">An <see cref="T:System.Net.IPAddress"/> that represents the local IP address. </param><param name="port">The port on which to listen for incoming connection attempts. </param><exception cref="T:System.ArgumentNullException"><paramref name="localaddr"/> is null. </exception><exception cref="T:System.ArgumentOutOfRangeException"><paramref name="port"/> is not between <see cref="F:System.Net.IPEndPoint.MinPort"/> and <see cref="F:System.Net.IPEndPoint.MaxPort"/>. </exception>
        public SmartTcpListener(IPAddress localaddr, int port)
            : base(localaddr, port)
        {
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Determines if the port is currently 
        /// </summary>
        /// <returns>true or false</returns>
        public bool IsEndPointListenedTo()
        {
            IPGlobalProperties ipGlobalProperties = IPGlobalProperties.GetIPGlobalProperties();
            List<IPEndPoint> tcpConnInfoArray = ipGlobalProperties.GetActiveTcpListeners().ToList();

            return tcpConnInfoArray.Any(endPoint => endPoint.ToString() == base.LocalEndpoint.ToString());
        }
        #endregion
    }
}

