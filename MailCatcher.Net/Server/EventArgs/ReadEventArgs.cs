﻿using System;

namespace MailCatcher.Net.Server.EventArgs
{
    /// <summary>
    /// Class for passing Read Event Arguments
    /// </summary>
    public class ReadEventArgs : System.EventArgs
    {
        #region Properties
        public string Data {get;set;}
        public SmtpServerState State {get;set;}
        public SmtpMessageCommands Command { get; set; }
        #endregion
    }
}

