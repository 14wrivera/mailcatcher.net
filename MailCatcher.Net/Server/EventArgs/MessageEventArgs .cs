﻿using System;
using System.Collections.Generic;

namespace MailCatcher.Net.Server.EventArgs
{
    /// <summary>
    /// Class for passing Read Event Arguments
    /// </summary>
    public class MessageEventArgs : System.EventArgs
    {
        #region Properties
        public IList<string> Message {get;set;}
        public IList<string> Headers { get; set; }
        #endregion
    }
}

