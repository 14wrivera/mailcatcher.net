﻿using System;
using System.Collections.Generic;

namespace MailCatcher.Net.Server
{
    /// <summary>
    /// This is the message. It will parse
    /// Other Properties when requested
    /// </summary>
    public class SmtpServerMessage
    {
        #region Properties
        public SmtpMessageCommands Command { get; set; }
        public string FullText { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// The Constructor with incoming text
        /// </summary>
        /// <param name="text">The incoming text</param>
        public SmtpServerMessage(string text, SmtpServerState serverState)
        {
            this.FullText = text;
            if (serverState == SmtpServerState.READING &&
                text != ".")
            {
                this.Command = SmtpMessageCommands.NA;
                return;
            }
            //MAP commands with Responses that come in
            Dictionary<string, SmtpMessageCommands> Commands = new Dictionary<string, SmtpMessageCommands>();
            Commands.Add("EHLO", SmtpMessageCommands.EHLO);
            Commands.Add("HELO", SmtpMessageCommands.EHLO);
            Commands.Add("MAIL FROM", SmtpMessageCommands.FROM);
            Commands.Add("RCPT TO", SmtpMessageCommands.TO);
            Commands.Add("DATA", SmtpMessageCommands.DATA);
            Commands.Add(".", SmtpMessageCommands.EOF);
            Commands.Add("QUIT", SmtpMessageCommands.QUIT);

            foreach (string command in Commands.Keys)
            {
                if (text.StartsWith(command))
                {
                    this.Command = Commands[command];
                    return;
                }
            };
            this.Command = SmtpMessageCommands.NA;
        }

        /// <summary>
        /// Base Constructor
        /// </summary>
        public SmtpServerMessage() { }
        #endregion
    }

    /// <summary>
    /// The Enum with possible Command Codes, Cleaner then
    /// trying to map strings
    /// </summary>
    public enum SmtpMessageCommands : int
    {
        EHLO = 1,
        FROM = 2,
        TO = 3,
        DATA = 4,
        EOF = 5,
        QUIT = 6,
        READING = 7,
        NA = 8,
        BROADCAST = 9
    }
}
