﻿using System.Collections.Generic;

namespace MailCatcher.Net.MailMessage
{
    /// <summary>
    /// MimeHeader class which helps with some identification
    /// of the headers
    /// </summary>
    public static class MimeHeader
    {
        #region Static Properties
        public readonly static Dictionary<string, int> Type =
        new Dictionary<string, int>{
            {"CONTENT-TYPE:",1},
            {"CONTENT-TRANSFER-ENCODING:",2},
            {"\tCHARSET=",1},
            {"CONTENT-DISPOSITION:",3},
            {"\tNAME=",1},
	        {"\tFILENAME=",3},
            {"CONTENT-ID:",4}

        };
        #endregion

        #region Static Public Methods
        /// <summary>
        /// Maps the line to a particular header
        /// </summary>
        /// <param name="data">the collection of string of text to test</param>
        /// <param name ="i">the index in the loop</param>
        /// <returns>int indicating the type</returns>
        public static int IsType(List<string> data, int i)
        {
            string line = data[i];
            int n = i - 1;
            if (n < 0) n = 0;
            string prev = data[n];

            if (line.ToUpper().StartsWith("CONTENT-TYPE:"))
            {
                return 1;
            }
            else if (line.ToUpper().StartsWith("\tCHARSET=") &&
                (prev.ToUpper().StartsWith("CONTENT-TYPE:"))
                )
            {
                return 1;
            }
            else if (line.ToUpper().StartsWith("CONTENT-TRANSFER-ENCODING:"))
            {
                return 2;
            }
            else if (line.ToUpper().StartsWith("CONTENT-DISPOSITION:"))
            {
                return 3;
            }
            else if (line.ToUpper().StartsWith("CONTENT-ID:"))
            {
                return 4;
            }
            else if (line.ToUpper().StartsWith("\tNAME=") &&
                prev.ToUpper().StartsWith("CONTENT-TYPE:"))
            {
                return 1;
            }
            else if (line.ToUpper().StartsWith("\tFILENAME=") &&
                prev.ToUpper().StartsWith("CONTENT-DISPOSITION:"))
            {
                return 3;
            }
            return 0;
        }
        /// <summary>
        /// Determines weather line of text starts with a 
        /// header
        /// </summary>
        /// <param name="text">the string of text to test</param>
        /// <returns>true/false</returns>
        public static bool IsHeader(string text)
        {
            if (text.ToUpper().StartsWith("CONTENT-TYPE:") ||
                text.ToUpper().StartsWith("CONTENT-TRANSFER-ENCODING:") ||
                text.ToUpper().StartsWith("CONTENT-ID:") ||
                text.ToUpper().StartsWith("\tCHARSET=") ||
                text.ToUpper().StartsWith("CONTENT-DISPOSITION:"))
                return true;
            return false;
        }
        #endregion
    }
}
