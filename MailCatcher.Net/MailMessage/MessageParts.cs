﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using MailCatcher.Net.MailMessage.Interfaces;
using MailCatcher.Net.MailMessage.MimeTypes;

namespace MailCatcher.Net.MailMessage
{
    public class MessageParts
    {
        #region Properties
        public bool IsMultiPart { get; set; }
        public IEnumerable<IMimeParts> Parts { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// The base constructor which takes a string of
        /// data and produces email parts
        /// </summary>
        /// <param name="message">The string of raw data</param>
        public MessageParts(string message)
        {
            string[] data = message.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.None);
            if (message.ToUpper().Contains("BOUNDARY="))
            {
                this.ExtractMultipart(data);
                this.IsMultiPart = true;
            }
            else
            {
                this.ExtractSinglePart(data);
                this.IsMultiPart = false;
            }
        }
        #endregion
        #region Public Methods
        /// <summary>
        /// This Searches through all parts for particular mime type
        /// </summary>
        /// <param name="type">this is the type that should be listed in
        /// lowercase fashion such as "text/html" </param>
        /// <returns>MessagePart</returns>
        public IMimeParts FindByContentType(string type)
        {
            return this.Parts.FindByContentType(type);
        }

        /// <summary>
        /// This Searches through all parts for particular mime type
        /// </summary>
        /// <param name="type">this is the type that should be listed in
        /// lowercase fashion such as "text/html" </param>
        /// <returns>MessagePart</returns>
        public IEnumerable<IMimeParts> RetrieveAttachments()
        {
            return this.Parts.FindAttachments();
        }

        #endregion
        #region Private Methods
        /// <summary>
        /// Extract parts from messages with multi parts
        /// </summary>
        /// <param name="data">The collection of data</param>
        private void ExtractMultipart(string[] data)
        {
            foreach (string line in data)
            {
                if (line.ToUpper().Contains("BOUNDARY="))
                {
                    this.Parts = this.extractPartByBoundary(line, data.ToList());
                    return;
                }
            }
        }
        /// <summary>
        /// Extracts the parts
        /// </summary>
        /// <param name="line">the actual line of data</param>
        /// <param name="data">The collection of data</param>
        /// <returns>Collection of mime parts</returns>
        private List<IMimeParts> extractPartByBoundary(string line, List<string> data)
        {
            Match match = Regex.Match(line, "\\=\"{0,1}(.*)(?:[\"]{0,1})");
            string flag = match.Groups[1].Value.Replace("\"",String.Empty);
            return this.ParseBoundary("--" + flag, "--" + flag + "--", data);
        }

        /// <summary>
        /// This parses the data for each mime part
        /// </summary>
        /// <param name="flag">the begin flag</param>
        /// <param name="stopFlag">the end flag</param>
        /// <param name="data">The collection of data</param>
        /// <returns>A collection of Mime Parts</returns>
        private List<IMimeParts> ParseBoundary(string flag, string stopFlag, List<string> data)
        {
            List<IMimeParts> parts = new List<IMimeParts>();
            bool start = false;
            List<string> read = new List<string>();
            int i = -1;
            do
            {
                i++;
                if ((data[i] == flag && start == true) || data[i] == stopFlag)
                {
                    parts.Add(this.Process(read));
                    read = new List<string>();
                }
                else if (data[i] == flag)
                {
                    start = true;
                }
                else if (start == true)
                {
                    read.Add(data[i]);
                }
            } while (data[i] != stopFlag);
            return parts;
        }

        /// <summary>
        /// Extract parts from messages that are not multipart
        /// </summary>
        /// <param name="data">The collection of data</param>
        private void ExtractSinglePart(string[] data)
        {
            List<string> items = new List<string>();
            bool flag = false;
            foreach (string item in data)
            {
                if (flag == true || MimeHeader.IsHeader(item))
                    items.Add(item);
                else if (String.IsNullOrWhiteSpace(item))
                    flag = true;
            }
            this.Parts = new List<IMimeParts> { this.Process(items) };
        }

        /// <summary>
        /// Processes the contents within a boundary
        /// </summary>
        /// <param name="data">The collection of data</param>
        /// <returns>returns the actual part</returns>
        private IMimeParts Process(List<string> data)
        {
            IMimeParts message = new MessagePart();
            for (int i = 0; i < data.Count; i++)
            {
                string line = data[i];
                if (line.ToUpper().Contains("BOUNDARY=") && i<3)
                {
                    message.SubParts = this.extractPartByBoundary(line, data);
                    return message;
                }
                else if (MimeHeader.IsType(data, i) == 1)
                {
                    message.ContentType += line;
                }
                else if (MimeHeader.IsType(data, i) == 2)
                {
                    message.ContentTransferEncoding += line;
                }
                else if (MimeHeader.IsType(data, i) == 3)
                {
                    message.ContentDisposition += line;
                }
                else if (MimeHeader.IsType(data, i) == 4)
                {
                    message.ContentID += line;
                }
                else
                {
                    message.Contents.Append(line + Environment.NewLine);
                }
            }
            return message;
        }
        #endregion
    }
}
