﻿using System.Collections.Generic;

using MailCatcher.Net.MailMessage.Interfaces;

namespace MailCatcher.Net.MailMessage
{
    /// <summary>
    /// Extensions for this list type
    /// </summary>
    public static class MimePartsListExtentions
    {
        /// <summary>
        /// This Searches through all parts for particular mime type
        /// </summary>
        /// <param name="type">this is the type that should be listed in
        /// lowercase fashion such as "text/html" </param>
        /// <returns>MessagePart</returns>
        public static IMimeParts FindByContentType(this IEnumerable<IMimeParts> parts, string type)
        {
            foreach (IMimeParts part in parts)
            {
                if (part.ContentType.ToLower().Contains(type))
                {
                    return part;
                }
                else if (part.SubParts != null)
                {
                    IMimeParts pspart = part.SubParts.FindByContentType(type);
                    if (pspart != null)
                        return pspart;
                }
            }
            return default(IMimeParts);
        }

        /// <summary>
        /// This Searches for all possible images and application aka
        /// attachments
        /// </summary>
        /// <returns>collection of IMimeParts aka attachment types</returns>
        public static List<IMimeParts> FindAttachments(this IEnumerable<IMimeParts> parts)
        {
            List<IMimeParts> attachments = new List<IMimeParts>();
            foreach (IMimeParts part in parts)
            {
                if (part.ContentType.ToLower().Contains("image") ||
                    part.ContentType.ToLower().Contains("application")
                    )
                {
                    attachments.Add(part);
                }
                else if (part.SubParts != null)
                {
                    List<IMimeParts> pspart = part.SubParts.FindAttachments();
                    if (pspart.Count > 1)
                        attachments.AddRange(pspart);
                    else if (pspart.Count == 1 && pspart[0].ContentType != null)
                        attachments.AddRange(pspart);
                }
            }
            return attachments;
        }
    }
}
