﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using MailCatcher.Net.MailMessage.Interfaces;

namespace MailCatcher.Net.MailMessage.MimeTypes
{
    /// <summary>
    /// The Generic Message part or Mime part 
    /// </summary>
    public class MessagePart : IMimeParts
    {
        #region Properties
        public string ContentType { get; set; }
        public string ContentTransferEncoding { get; set; }
        public string ContentDisposition { get; set; }
        public string ContentID { get; set; }
        public IEnumerable<IMimeParts> SubParts { get; set; }
        public StringBuilder Contents { get; set; }
        #endregion

        public MessagePart()
        {
            this.Contents = new StringBuilder();
        }

        #region Public Methods
        /// <summary>
        /// Retrieve Contents
        /// </summary>
        /// <returns>String of contents of the message in the correct output</returns
        public string RetrieveContents()
        {
            if (this.ContentTransferEncoding.ToLower().Contains("quoted-printable"))
            {
                string txt = Regex.Replace(this.Contents.ToString(), @"=([0-9a-fA-F]{2})|=\r\n",
                  m => m.Groups[1].Success
                       ? Convert.ToChar(Convert.ToInt32(m.Groups[1].Value, 16)).ToString()
                       : "");
                return txt.Trim();
            }
            else
            {
                return this.Contents.ToString().Trim();
            }
        }
        #endregion
    }
}
