﻿using System.Collections.Generic;
using System.Text;

namespace MailCatcher.Net.MailMessage.Interfaces
{
    public interface IMimeParts
    {
        string ContentType { get; set; }
        string ContentTransferEncoding { get; set; }
        string ContentDisposition { get; set; }
        string ContentID { get; set; }
        StringBuilder Contents { get; set; }
        IEnumerable<IMimeParts> SubParts { get; set; }
        string RetrieveContents();
    }
}
